#include "LinkedListNode.h"
#include <iostream>

linkedlistnode::linkedlistnode(char *data)
{
    this->data = data;
    this->next = NULL;
    this->prev = NULL;
}
void linkedlistnode::setNext(linkedlistnode *next)
{
    this->next = next;
}
void linkedlistnode::setPrev(linkedlistnode *prev)
{
    this->prev = prev;
}

char *linkedlistnode::getData()
{
    return this->data;
}

linkedlistnode *linkedlistnode::getNext()
{
    return this->next;
}

linkedlistnode *linkedlistnode::getPrev()
{
    return this->prev;
}