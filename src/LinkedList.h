#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#include "LinkedListNode.h"
class linkedlist
{
private:
  linkedlistnode *head;
  linkedlistnode *tail;
  int size;

public:
  linkedlist();
  void add(char *data);
  void remove(unsigned int index);
  char *get(unsigned int index);
  void getList();
  int getSize();
};
#endif