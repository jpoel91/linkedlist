#include <iostream>
#include "LinkedList.h"
#include "LinkedListNode.h"

linkedlist::linkedlist()
{
    //Create header and tail
    head = new linkedlistnode(NULL);
    tail = new linkedlistnode(NULL);

    head->setNext(tail);
    tail->setPrev(head);

    size = 0;
}

void linkedlist::remove(unsigned int index)
{
    //Check if index is right.
    if (index > size || size == 0)
    {
        std::cout << "Index bigger than listsize!";
        return;
    }

    //Calculate which node must be removed.
    linkedlistnode *prevNode, *nextNode, *cur;

    if (index < (size / 2))
    {
        cur = head->getNext();

        for (int i = 1; i < index; i++)
        {
            cur = cur->getNext();
        }
    }
    else
    {
        cur = tail->getPrev();
        for (int i = size; i > index; i--)
        {
            cur = cur->getPrev();
        }
    }

    //Remove the node
    prevNode = cur->getPrev();
    nextNode = cur->getNext();

    prevNode->setNext(nextNode);
    nextNode->setPrev(prevNode);

    size--;
}

char *linkedlist::get(unsigned int index)

{

    linkedlistnode *cur;

    char *data;
    //Check if index is valid
    if (index > size || size == 0)
    {
        std::cout << "Index bigger than listsize!";
        return NULL;
    }

    if (index < (size / 2))
    {
        cur = head->getNext();
        data = cur->getData();

        for (int i = 0; i < index; i++)
        {
            cur = cur->getNext();
            data = cur->getData();
        }
    }
    else
    {

        cur = tail->getPrev();
        data = cur->getData();

        for (int i = (size - 1); i > index; i--)
        {
            cur = cur->getPrev();
            data = cur->getData();
        }
    }

    return data;
}

void linkedlist::add(char *data)
{

    linkedlistnode *newNode, *prevNode;

    //Create new node
    newNode = new linkedlistnode(data);

    //Calculate prev node.
    prevNode = tail->getPrev();

    //Set newNode's next to the tail
    newNode->setNext(tail);

    //Set newNode's prev to prevNode.
    newNode->setPrev(prevNode);

    //Set the prev node's next to the newNode
    prevNode->setNext(newNode);

    //Assign tail's prev to newNode
    tail->setPrev(newNode);

    //add up size
    size++;
}

int linkedlist::getSize()
{
    return size;
}

void linkedlist::getList()
{
    linkedlistnode *cur = head->getNext();
    while (cur != tail)
    {
        std::cout << cur->getData() << std::endl;
        cur = cur->getNext();
    }
}