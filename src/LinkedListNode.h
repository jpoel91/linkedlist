#ifndef NODE_H
#define NODE_H
class linkedlistnode
{
private:
  linkedlistnode *prev;
  linkedlistnode *next;
  char *data;

public:
  linkedlistnode(char *data);
  void setPrev();
  void setNext(linkedlistnode *next);
  void setPrev(linkedlistnode *prev);
  char *getData();
  linkedlistnode *getNext();
  linkedlistnode *getPrev();
};

#endif