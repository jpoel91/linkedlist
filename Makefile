# Generated by pymake version 0.5
# PyMake was written by Stephen Melinyshyn | github.com/Melinysh/PyMake

CXX := g++
CXXFLAGS := 
PYMAKE_COMPILER := $(CXX)
PYMAKE_COMPILER_FLAGS := $(CXXFLAGS)
SRCEXT := cpp
SRCDIR := src
BUILDDIR := out
INSTALL_PATH := /usr/local/bin
TARGET := main
SOURCES := $(wildcard $(SRCDIR)/*.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%.o,$(BUILDDIR)/%.o,$(SOURCES:.$(SRCEXT)=.o))


all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(PYMAKE_COMPILER) -o $(TARGET) $^

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	$(PYMAKE_COMPILER) $< $(PYMAKE_COMPILER_FLAGS) -c -o $@

clean:
	-rm $(TARGET) $(OBJECTS)

run: all
	./$(TARGET)

install: $(TARGET)
	install $(TARGET) $(INSTALL_PATH)

uninstall:
	-rm $(INSTALL_PATH)/$(TARGET)
